import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

class  Counter{

    int count = 0;
    long threadId = 999999;

    void  add(){
        if(threadId != Thread.currentThread().getId()){
            threadId = Thread.currentThread().getId();
        }else{
            System.out.println("Error : Same thread id cannot proceed");
            return;
        }

        for (int i = 0; i < 100000; i++) {
            count++;

        }
        try{
            Thread.sleep(1);
        }catch(Exception e){

        }
    }

    int getTotalCount(){
        return count;
    }
}


public class Solution {

    public static void main(String[] args)throws InterruptedException {
        Scanner scan = new Scanner(System.in);
        int intiger = scan.nextInt();
        Counter counter = new Counter();
        while(counter.getTotalCount()<intiger)
        {
            Thread ob =new Thread(()->counter.add());
            ob.start();
            Thread.sleep(5);
        }
        System.out.println(counter.getTotalCount());

    }
}